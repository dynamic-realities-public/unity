﻿using System;
using UnityEditor;
using UnityEngine;

namespace DynamicRealities.Unity.Editor
{
	/// <summary>
    /// Class used to give a "Replace GameObject" menu option under the "GameObject" general menu.
    /// This will extend the editor to allow a user to replace any amount of in-scene objects with
    /// another object instead. This is useful for when replacing one prefab with a prefab variant,
    /// or a completely different object entirely.
    /// Every object replaced with this class retains its position, rotation and scale.
    /// NOTE: Once the command is run, there is no Ctrl + Z action to take. So make sure you want
    /// the action to happen in the first place before you pick the object to use as a replacement.
    /// </summary>
    public class SceneObjectReplacementContextMenu
    {
        private static GameObject[] selectedObjects;
        private static UnityEngine.Object replacementObject;

        [MenuItem("GameObject/Replace GameObject", false, 0)]
        public static void ReplacePrefabCall()
        {
            if (Selection.objects.Length == 0)
            {
                return;
            }

            selectedObjects = Selection.gameObjects;
            replacementObject = null;
            Selection.objects = new UnityEngine.Object[0];

            var currentPickerwindow = EditorGUIUtility.GetControlID(FocusType.Passive) + 100;
            EditorGUIUtility.ShowObjectPicker<GameObject>(null, false, "", currentPickerwindow);

            EditorApplication.hierarchyWindowItemOnGUI += GuiHandler;
        }

        private static void GuiHandler(int instanceID, Rect selectionRect)
        {
            if (Event.current.commandName == "ObjectSelectorUpdated")
            {
                var newObject = EditorGUIUtility.GetObjectPickerObject();
                if (replacementObject != newObject)
                {
                    replacementObject = newObject;
                }
            }

            if (Event.current.commandName == "ObjectSelectorClosed")
            {
                if (replacementObject != null)
                {
                    ReplaceObjects();
                }
                EditorApplication.hierarchyWindowItemOnGUI -= GuiHandler;
            }
        }

        private static void ReplaceObjects()
        {
            int replacedObjects = 0;
            try
            {
                if (selectedObjects != null && selectedObjects.Length > 0)
                {
                    foreach (GameObject obj in selectedObjects)
                    {
                        Transform transform = obj.transform;

                        GameObject newObject = (GameObject)PrefabUtility.InstantiatePrefab(replacementObject, transform.parent);
                        newObject.name = obj.name;
                        newObject.transform.position = transform.position;
                        newObject.transform.rotation = transform.rotation;
                        newObject.transform.localScale = transform.localScale;
                        replacedObjects += 1;
                    }

                    for (int index = selectedObjects.Length - 1; index > -1; index--)
                    {
                        UnityEngine.Object.DestroyImmediate(selectedObjects[index]);
                    }
                    Debug.Log($"Successfully replaced {replacedObjects} GameObjects in the scene.");
                }
            }
            catch (Exception ex)
            {
                if (replacedObjects > 0)
                {
                    Debug.Log($"Successfully replaced {replacedObjects} GameObjects in the scene.");
                }
                Debug.LogError("[SceneObjectReplacementContextMenu]: An exception occurred while calling ReplaceObjects():");
                Debug.LogException(ex);
            }
        }
    }
}